<!DOCTYPE html>
<html lang="en" class="no-js"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ACPE - Agence Congolaise Pour l'Emploi</title>
    <meta name="description" content="ACPE-Agence Congolaise Pour l'Emploi" />
	<meta name="keywords" content="Agence Emploi, Agence Emploi, Agence Emploi, Congo, Brazzville" />
	<meta name="author" content="acpe.cg" />

    <link rel="shortcut icon" href="{{ asset('images/favicon_io/favicon-32x32.png') }}">

    <!-- google font -->
    <link href="{{ asset('FrontEnd')}}/css/css.css" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{ asset('FrontEnd')}}/css/bootstrap.min.css" type="text/css">

    <!--Material Icon -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset('FrontEnd')}}/css/fontawesome.css">

    <!-- selectize css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('FrontEnd')}}/css/selectize.css">

    <!--Slider-->
    <link rel="stylesheet" href="{{ asset('FrontEnd')}}/css/owl.carousel.css">
    <link rel="stylesheet" href="{{ asset('FrontEnd')}}/css/owl.theme.css">
    <link rel="stylesheet" href="{{ asset('FrontEnd')}}/css/owl.transitions.css">

    <!-- Custom  Css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('FrontEnd')}}/css/style.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('FrontEnd')}}/css/main-carousel.css">
</head>

<body>
    <!-- Navigation Bar-->
    <header id="topnav" class="defaultscroll scroll-active active scroll">
        <div class="container-fluid p-2">
            <!-- Logo container-fluid-->
            <div>
                <a href="#" class="logo mb-2">
                    <img src="{{ asset('FrontEnd')}}/images/logo-light.png" alt="" class="logo-light" height="60">
                    <img src="{{ asset('FrontEnd')}}/images/logo-light.png" alt="" class="logo-dark" height="60">
                </a>
            </div>
            <!-- End Logo container-fluid-->
            <div class="menu-extras">

                <div class="menu-item">
                    <!-- Mobile menu toggle-->
                    <a class="navbar-toggle">
                        <div class="lines">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </a>
                    <!-- End mobile menu toggle-->
                </div>
            </div>

            <div id="navigation" class="active">

                <!-- Navigation Menu-->
                <ul class="navigation-menu justify-content-center ">

                    
                    <li class=" last-elements">
                        <a  class="text-capitalize" href="#">Emplois</a>
                    </li>
                    <li class="last-elements">
                        <a class="text-capitalize" href="#">Actualités</a>
                    </li>
                    <li class=" last-elements">
                        <a class="text-capitalize" href="#">Conseils</a>
                    </li>

                    <li class="last-elements ">
                        <a class="text-uppercase" href="#">ACPE</a>
                    </li>

                    <li class="last-elements">
                        <a  class="text-capitalize" href="#">Espace client</a>
                    </li>
                    <li class="last-elements">
                        <a class="text-capitalize" href="#">Espace Entreprise</a>
                    </li>
                </ul>
                <!-- End navigation menu-->
            </div>
        </div>
    </header>
    <!-- End Navigation Bar-->

    <!-- Search & Carousel -->
    <section id="carousel">
    </section>
    <!-- End of search & carousel -->

    <!-- Block of direction -->
    <section id="ac-direct" class="" >
    <div class="d-flex flex-row" style="height: 250px;">
        <div class="col-lg-6 col-md-6 h-100 p-2" style=" border-bottom: 1px solid rgba(0,0,0,.05);">
                <div class="grid-list-desc text-center mt-4">
                        <h3 class="mb-1"><a href="#" class="text-center">Candidat ?</a></h3>
                        <p class="text-muted f-14 mb-0">Connectez-vous pour postuler</p>
                                <p class="text-muted f-14 mb-0">Enregistrer les offres 
                                        intéressantes &
                                                Gérer votre compte</p>
                        <div class="apply-button text-center mt-2 mb-2">
                                <a href="#" class="btn btn-white  btn-sm">Espace candidat</a>
                            </div>
                    </div>
        </div>
        <div id="ac-enterprise" class="col-lg-6 col-md-6 h-100 p-2">
                <div class="grid-list-desc text-center mt-4">
                        <h3 class="mb-1"><a href="#" class="text-center">Entreprise ?</a></h3>
                        <p class="text-muted f-14 mb-0">Connectez-vous pour poster une offre d’emploi
                                </p>
                                <p class="text-muted f-14 mb-0">
                                        &</p>
                                <p class="text-muted f-14 mb-0">
                                        Gérer votre compte</p>
                        <div class="apply-button text-center mt-2 mb-2">
                                <a href="#" class="btn btn-white btn-sm">Espace entreprise</a>
                            </div>
                    </div>
        </div>
    </div>
    </section>
    <!-- End of dicrection-->

    <!-- News Snippets -->
    <section id="ac-news" style="border-top: 0.5px #F8F9FA  solid;">
            <div class="container">
        <div class="container-fluid " style="min-height: 600px;">
            <div class="flex-column">
                <div class="d-flex flex-row h-auto justify-content-center ">
                    <h3 class="text-uppercase mb-5 mt-5">Actualités</h3>
                </div>

                <div class="d-flex flex-row  justify-content-between mt-2 mb-5" style="height: 300px;">
                    <div class="d-flex flex-column shadow-sm mb-5 bg-white w-25 h-100 ac-new-b">
                            <span class="h-auto pl-4 p-1 text-left">
                                    <p>Comment trouver du boulot facilement ?</p>
                                </span>
                        <img src="{{URL::asset('/FrontEnd/images/cp-pr.jpg')}}" alt="" class="h-75">
                        
                    </div>
                    <div class="d-flex flex-column shadow-sm mb-5 bg-white w-25 h-100 ac-new-b">
                            <img src="{{URL::asset('/FrontEnd/images/cp-pr.jpg')}}" alt="" class="h-75 border-bottom ">
                            <span class="h-auto p-1 text-center">
                                <p>Comment trouver du boulot facilement ?</p>
                            </span>
                        </div>
                        <div class="d-flex flex-column shadow-sm mb-5 bg-white w-25 h-100 ac-new-b">
                                <img src="{{URL::asset('/FrontEnd/images/cp-pr.jpg')}}" alt="" class="h-75 border-bottom ">
                                <span class="h-auto p-1 text-center">
                                    <p>Comment trouver du boulot facilement ?</p>
                                </span>
                            </div>
                        
                </div>
                <div class="d-flex flex-row  justify-content-between mt-2" style="height: 300px;">
                        <div class="d-flex flex-column shadow-sm mb-5 bg-white w-25 h-100 ac-new-b">
                                <img src="{{URL::asset('/FrontEnd/images/cp-pr.jpg')}}" alt="" class="h-75 border-bottom ">
                                <span class="h-auto p-1 text-center">
                                    <p>Comment trouver du boulot facilement ?</p>
                                </span>
                            </div>
                            <div class="d-flex flex-column shadow-sm mb-5 bg-white w-25 h-100 ac-new-b">
                                    <img src="{{URL::asset('/FrontEnd/images/cp-pr.jpg')}}" alt="" class="h-75 border-bottom ">
                                    <span class="h-auto p-1 text-center">
                                        <p>Comment trouver du boulot facilement ?</p>
                                    </span>
                                </div>
                                <div class="d-flex flex-column shadow-sm mb-5 bg-white w-25 h-100 ac-new-b">
                                        <img src="{{URL::asset('/FrontEnd/images/cp-pr.jpg')}}" alt="" class="h-75 border-bottom ">
                                        <span class="h-auto p-1 text-center">
                                            <p>Comment trouver du boulot facilement ?</p>
                                        </span>
                                    </div>
                            
                </div>

                <div class="d-flex flex-row  justify-content-center mt-2" style="height: 100px;">
                        <div class="apply-button text-center mt-5 mb-5">
                                <a href="#" class="btn btn-load btn-white btn-sm">Plus d'actualités</a>
                        </div>
                </div>
            </div>
        </div></div>
    </section>
    <!-- End of News Snippets -->


    <!-- Conseils Area -->
    <!-- News Snippets -->
    <section id="ac-news" style="border-top: 0.5px #F8F9FA  solid;">
            <div class="container-fluid " style="min-height: 600px;">
                <div class="flex-column">
                    <div class="d-flex flex-row h-auto justify-content-center ">
                        <h3 class="text-uppercase mb-5 mt-5">Conseils</h3>
                    </div>
    
                    <div class="d-flex flex-row  justify-content-between mt-2 mb-5  p-2 mb-5" style="height: 300px;">
                        
                        <div class="d-flex flex-column  bg-white m-1 w-25 h-100 ac-new-b">
                                <img src="{{URL::asset('/FrontEnd/images/cp-pr.jpg')}}" alt="" class="h-75 border-bottom ">
                                <span class="h-auto p-1 text-center">
                                    <p>Comment trouver du boulot facilement ?</p>
                                </span>
                            </div>
                            <div class="d-flex flex-column bg-white w-25 h-100 m-1 ac-new-b">
                                    <img src="{{URL::asset('/FrontEnd/images/cp-pr.jpg')}}" alt="" class="h-75 border-bottom ">
                                    <span class="h-auto p-1 text-left">
                                        <p>Comment trouver du boulot facilement ?</p>
                                    </span>
                                </div>
                                <div class="d-flex flex-column bg-white w-25 m-1 h-100 ac-new-b">
                                        <img src="{{URL::asset('/FrontEnd/images/cp-pr.jpg')}}" alt="" class="h-75 border-bottom ">
                                        <span class="h-auto p-1 text-left">
                                            <p>Comment trouver du boulot facilement ?</p>
                                        </span>
                                    </div>
                                    <div class="d-flex flex-column bg-white m-1 w-25 h-100 ac-new-b">
                                            <img src="{{URL::asset('/FrontEnd/images/cp-pr.jpg')}}" alt="" class="h-75 border-bottom ">
                                            <span class="h-auto p-1 text-left">
                                                <p>Comment trouver du boulot facilement ?</p>
                                            </span>
                                        </div>
                            
                    </div>
                    
    
                    <div class="d-flex flex-row  justify-content-center mt-2" style="height: 100px;">
                            <div class="apply-button text-center mt-1 mb-2">
                                    <a href="#" class="btn btn-load btn-white btn-sm">Plus de conseils</a>
                            </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End of conseils area -->
    <section>
    <div class="container-fluid" style="background: #F8F9FA !important;">
            <div class="d-flex flex-row justify-content-lg-center">
                    <div class="pl-2 pr-2 pb-2 pt-0" style="height: 40px; background: #000;">
                        <h2 class="h-auto text-uppercase text-white">
                            +
                        </h2>
                    </div>
                    
                </div>
    </div>
    </section>
    <!-- footer start -->
    <footer class="footer pt-5">
        <div class="container-fluid">
            
        </div>
    </footer>
    <!-- footer end -->

    <!-- footer-alt start -->
    <section class="footer-alt pt-3 pb-3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <p class="copyright mb-0">COPYRIGHT 2019 &copy; ACPE</p>
                </div>
            </div>
        </div>
    </section>
    <!-- footer-alt end -->

    <!-- javascript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="{{ asset('FrontEnd')}}/js/bootstrap.bundle.min.js"></script>
    <script src="{{ asset('FrontEnd')}}/js/jquery.easing.min.js"></script>
    <script src="{{ asset('FrontEnd')}}/js/plugins.js"></script>

    <!-- selectize js -->
    {{-- <script src="{{ asset('FrontEnd')}}/js/selectize.min.js"></script> --}}
    <script src="{{ asset('FrontEnd')}}/js/jquery.nice-select.min.js"></script>

    <script src="{{ asset('FrontEnd')}}/js/owl.carousel.min.js"></script>
    <script src="{{ asset('FrontEnd')}}/js/counter.int.js"></script>

    <script src="{{ asset('FrontEnd')}}/js/app.js"></script>
    <script src="{{ asset('FrontEnd')}}/js/main-carousel.js"></script>
    <script src="{{ asset('FrontEnd')}}/js/home.js"></script>


</body></html>