<!DOCTYPE html>
<!-- saved from url=(0041)https://themesdesign.in/joobsy/index.html -->
<html lang="en" class="no-js"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ACPE - Agence Congolaise Pour l'Emploi</title>
    <meta name="description" content="ACPE-Agence Congolaise Pour l'Emploi" />
	<meta name="keywords" content="Agence Emploi, Agence Emploi, Agence Emploi, Congo, Brazzville" />
	<meta name="author" content="acpe.cg" />

    <link rel="shortcut icon" href="{{ asset('images/favicon_io/favicon-32x32.png') }}">

    <!-- google font -->
    <link href="{{ asset('FrontEnd')}}/css/css.css" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{ asset('FrontEnd')}}/css/bootstrap.min.css" type="text/css">

    <!--Material Icon -->
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('FrontEnd')}}/css/materialdesignicons.min.css"> --}}
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset('FrontEnd')}}/css/fontawesome.css">

    <!-- selectize css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('FrontEnd')}}/css/selectize.css">

    <!--Slider-->
    <link rel="stylesheet" href="{{ asset('FrontEnd')}}/css/owl.carousel.css">
    <link rel="stylesheet" href="{{ asset('FrontEnd')}}/css/owl.theme.css">
    <link rel="stylesheet" href="{{ asset('FrontEnd')}}/css/owl.transitions.css">

    <!-- Custom  Css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('FrontEnd')}}/css/style.css">
    <style>
        header#topnav.defaultscroll.scroll-active.active.scroll{
            border-bottom: #1f72cf 2px solid !important;
        }
        div#carouser a.btn.active.btn-block.pricing-btn.btn-outline {
            background-color: #1a99fd !important;
            border-radius: 0px !important;
            border-color: none !important;
            color: #fff !important;
        }
        div#carouser a.btn.btn-block.pricing-btn.btn-outline  {
            color: #1a99fd !important;
    border-color: #1a99fd !important;
    border-radius: 0px !important;
        }
        div#carouser a.btn.btn-block.pricing-btn.btn-outline:hover, div#carouser a.btn.btn-block.pricing-btn.btn-outline:focus {
            background-color: #1a99fd !important;
            color: #fff !important;
        }
        section#tampon.bg-home{
        background-image: url("{{URL::asset('/FrontEnd/images/bg-us.jpg')}}") !important;
        padding: 80px 0px 135px 0px !important;
        background-size: cover !important;
        position: relative;
        background-position: center center !important;
        background-position-y: 0 !important;
        height: 730px !important;
        }
        section#tampon div.bg-overlay{
        background-color: #ffffff !important;
        }

        section#profilesec.bg-home{
            background-image: url("{{URL::asset('/FrontEnd/images/bg-us.jpg')}}") !important;
            padding: 80px 0px 135px 0px !important;
            background-size: cover !important;
            position: relative;
            background-position: center center !important;
            background-position-y: 0 !important;
            height: 730px !important;
        }
        section#profilesec div.bg-overlay{
            background-color: #ffffff !important;
        }
        section#profilect .home-title h1 {
            letter-spacing: 2px;
            font-size: 3rem;
            text-shadow : 0px 0px 0px !important;
            /* text-shadow: 3px 2px 7px rgba(26, 24, 24, 0.7); */
        }

    </style>
</head>

<body>

    <!-- Navigation Bar-->
    <header id="topnav" class="defaultscroll scroll-active active scroll">
        {{-- <div class="tagline">
            <div class="container">
                <div class="float-left">
                    <div class="phone">
                        <i class="mdi mdi-phone-classic"></i> +1 800 123 45 67
                    </div>
                    <div class="email">
                        <a href="https://themesdesign.in/joobsy/index.html#">
                            <i class="mdi mdi-email"></i> Support@mail.com
                        </a>
                    </div>
                </div>
                <div class="float-right">
                    <ul class="topbar-list list-unstyled d-flex" style="margin: 11px 0px;">
                        <li class="list-inline-item"><a href="javascript:void(0);"><i class="mdi mdi-account mr-2"></i>Benny Simpson</a></li>
                        <li class="list-inline-item">
                            <a href="javascript:void(0);">
                                <select id="select-lang" class="demo-default selectized" placeholder="Language" tabindex="-1" style="display: none;"><option value="" selected="selected"></option></select><div class="selectize-control demo-default single"><div class="selectize-input items not-full has-options"><input type="select-one" autocomplete="off" tabindex="" id="select-lang-selectized" placeholder="Language" style="width: 60.9531px;"></div></div>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        </div> --}}
        <div class="container p-2">
            <!-- Logo container-->
            <div>
                <a href="https://themesdesign.in/joobsy/index.html" class="logo mb-2">
                    <img src="{{ asset('FrontEnd')}}/images/logo-dark.png" alt="" class="logo-light" height="100">
                    <img src="{{ asset('FrontEnd')}}/images/logo-light.png" alt="" class="logo-dark" height="100">
                </a>
            </div>
            <!-- End Logo container-->
            <div class="menu-extras">

                <div class="menu-item">
                    <!-- Mobile menu toggle-->
                    <a class="navbar-toggle">
                        <div class="lines">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </a>
                    <!-- End mobile menu toggle-->
                </div>
            </div>

            <div id="navigation" class="active">

                <!-- Navigation Menu-->
                <ul class="navigation-menu">

                    <li class="has-submenu active">
                        <a href="https://themesdesign.in/joobsy/index.html">ACPE</a>
                    </li>
                    <li class="has-submenu last-elements">
                        <a href="#">Actualités</a>
                    </li>
                    <li class="has-submenu last-elements">
                        <a href="#">Emplois</a>
                    </li>
                    <li class="has-submenu last-elements">
                        <a href="#">Conseils</a>
                    </li>

                    <li class="has-submenu last-elements">
                        <a href="#">contact</a>
                    </li>
                    <a href="#" class="btn btn-custom btn-sm"><i class="mdi mdi-cloud-upload"></i> CONNEXION</a>

                </ul>
                <!-- End navigation menu-->
            </div>
        </div>
    </header>
    <!-- End Navigation Bar-->


    <!-- SING IN FORM START -->
    <section class="form-bg">
        <div class="modal fade" id="ModalCenter1" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">REGISTER</h5>
                        <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="text-center">
                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                            <label class="btn btn-primary">
                                <input type="radio" name="options" id="option2" autocomplete="off"> Candidate
                            </label>

                            <label class="btn btn-primary">
                                <input type="radio" name="options" id="option3" autocomplete="off"> Employer
                            </label>
                        </div>
                                </div>
                            </div>
                        </div>
                        <div class="custom-form mt-4">
                            <form>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group blog-details-form">
                                            <i class="mdi mdi-account text-muted f-17"></i>
                                            <input name="name1" id="name1" type="text" class="form-control blog-details f-15 pt-2" placeholder="User Name" required="">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group blog-details-form">
                                            <i class="mdi mdi-email text-muted f-17"></i>
                                            <input name="email" id="email" type="text" class="form-control blog-details f-15 pt-2" placeholder="Email" required="">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group blog-details-form">
                                            <i class="mdi mdi-lock text-muted f-17"></i>
                                            <input name="password1" id="password1" type="Password" class="form-control blog-details f-15 pt-2" placeholder="Password" required="">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group blog-details-form">
                                            <i class="mdi mdi-lock text-muted f-17"></i>
                                            <input name="confirm-password" id="confirm-password" type="Password" class="form-control blog-details f-15 pt-2" placeholder="Confirm Password" required="">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <ul class="list-inline mb-0">
                                            <li class="list-inline-item">
                                                <div class="custom-control custom-checkbox pl-0 mb-1 mt-1">
                                                    <input type="checkbox" class="custom-control-input" id="customCheck2">
                                                    <label class="custom-control-label ml-1 text-muted f-15" for="customCheck2">Remember Me</label>
                                                </div>
                                            </li>
                                            <li class="list-inline-item float-right">
                                                <p class="mb-0"><a href="https://themesdesign.in/joobsy/index.html" class="text-dark">Lost your password?</a></p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-sm-12 text-right">
                                        <input type="submit" id="submit3" name="submit" class="btn-block btn btn-custom" value="Register Now">
                                    </div>
                                </div>

                                <div class="job-single-or mt-4 mb-4 position-relative">
                                    <h5 class="mb-0 text-dark text-center">OR</h5>
                                </div>
                                <ul class="list-inline text-center">
                                    <li class="list-inline-item mr-1">
                                        <a href="https://themesdesign.in/joobsy/index.html" class="text-white">
                                            <div class="sing-form-icon bg-primary rounded">
                                                <h6 class="mb-0"><i class="mdi mdi-facebook mr-1"></i>Facebook</h6>
                                            </div>
                                        </a>
                                    </li>

                                    <li class="list-inline-item mr-1">
                                        <a href="https://themesdesign.in/joobsy/index.html" class="text-white">
                                            <div class="sing-form-icon bg-info rounded">
                                                <h6 class="mb-0"><i class="mdi mdi-twitter mr-1"></i>Twitter</h6>
                                            </div>
                                        </a>
                                    </li>

                                    <li class="list-inline-item mr-1">
                                        <a href="https://themesdesign.in/joobsy/index.html" class="text-white">
                                            <div class="sing-form-icon bg-danger rounded">
                                                <h6 class="mb-0"><i class="mdi mdi-google-plus mr-1"></i>Google</h6>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </form>
                            <!-- /form -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- SING IN FORM END -->

    <section class="bg-home">
        <div class="bg-overlay"></div>
        <div class="home-center">
            <div class="home-desc-center">
                <div class="container">
                    <div class="row justify-content-center">
                            <div class="col-lg-9 " style="height: 215px;">
                                    <div class="home-title text-center text-white"></div></div>
                    </div>
                    <div id="carouser" class="home-form-position">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="home-registration-form p-4 mb-3">
                                    <form class="registration-form">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-6 ">
                                                <div class="registration-form-box">
                                                    <div class="text-center mt-2">
                                                            <a href="#" class="btn active btn-block pricing-btn btn-outline text-capitalize">Recherche</a>
                                                        </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-6 ">
                                                <div class="registration-form-box">
                                                        <div class="text-center mt-2">
                                                                <a href="#" class="btn  btn-block pricing-btn btn-outline text-capitalize">Actualités</a>
                                                            </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-6 ">
                                                <div class="registration-form-box">
                                                        <div class="text-center mt-2">
                                                                <a href="#" class="btn  btn-block pricing-btn btn-outline text-capitalize">Assistance</a>
                                                            </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <p class="text-white-50 mb-0 d-none"><span class="text-white">keywords :</span> Web designer, UI/UX designer, Graphic designer, Developer, PHP, Call center...</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end home -->
    
    <!-- Description start -->
    <section id="tampon" class="bg-home">
        <div class="bg-overlay"></div>
        <div class="home-center">
                <div class="home-desc-center">
                    <div class="container">
                        {{-- <div class="row justify-content-center">
                                <div class="col-lg-9 " style="height: 215px;">
                                        <div class="home-title text-center text-white"></div></div>
                        </div> --}}
                                <div   id="descriptor" class="d-flex flex-row justify-content-end">
                                <div class="col-lg-4 ">
                                            <div class="p-2 h-100 no-gutters">
                                                    <img src="{{URL::asset('/FrontEnd/images/cp-pr.jpg')}}" class="img-fluid" alt="ACPE image">
                                            </div>
                                </div>
                                <div class="col-lg-6 border">
                                        <div class="p-2">Description ici</div>
                            </div>
                            </div>
                    </div>
                </div>
            </div>
    </section>
    <!-- End of Description start -->

    <!-- Description start -->
    <section id="profilesec" class="bg-home">
        <div class="bg-overlay"></div>
        <div class="home-center">
                <div class="home-desc-center">
                    <div class="container">
                        <div class="row justify-content-center">
                                <div class="col-lg-9 " style="height: 215px;">
                                        <div class="home-title text-center text-white">
                                            <h1>Cherchez un emploi</h1>
                                        </div>
                                </div>
                        </div>
                        <div id="profilect" class="d-flex flex-row justify-content-center border-primary" style="height: 435px;">
                                <div class="col-lg-8 h-100 bg-white" >
                                         #Here Multiple Container   
                                </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <!-- End of Description start -->

    <!-- footer start -->
    <footer class="footer pt-5">
        <div class="container">
            <div class="row">
                ok
            </div>
        </div>
    </footer>
    <!-- footer end -->

    <!-- footer-alt start -->
    <section class="footer-alt pt-3 pb-3">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <p class="copyright mb-0">2019 © ACPE.</p>
                </div>
            </div>
        </div>
    </section>
    <!-- footer-alt end -->

    <!-- javascript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="{{ asset('FrontEnd')}}/js/bootstrap.bundle.min.js"></script>
    <script src="{{ asset('FrontEnd')}}/js/jquery.easing.min.js"></script>
    <script src="{{ asset('FrontEnd')}}/js/plugins.js"></script>

    <!-- selectize js -->
    {{-- <script src="{{ asset('FrontEnd')}}/js/selectize.min.js"></script> --}}
    <script src="{{ asset('FrontEnd')}}/js/jquery.nice-select.min.js"></script>

    <script src="{{ asset('FrontEnd')}}/js/owl.carousel.min.js"></script>
    <script src="{{ asset('FrontEnd')}}/js/counter.int.js"></script>

    <script src="{{ asset('FrontEnd')}}/js/app.js"></script><div class="selectize-dropdown single demo-default" style="display: none; width: 106px; top: 41px; left: 1082px;"><div class="selectize-dropdown-content"></div></div><span style="position: absolute; top: -99999px; left: -99999px; width: auto; padding: 0px; white-space: pre; letter-spacing: 0px; font-size: 13px; font-family: Roboto, sans-serif; font-weight: 400; text-transform: none;">Search Location...</span><div class="selectize-dropdown single demo-default" style="display: none; width: 243px; top: 511px; left: 382px; visibility: visible;"><div class="selectize-dropdown-content"><div class="option" data-selectable="" data-value="AF">Afghanistan</div><div class="option selected" data-selectable="" data-value="AX">Åland Islands</div><div class="option selected active" data-selectable="" data-value="AL">Albania</div><div class="option" data-selectable="" data-value="DZ">Algeria</div><div class="option" data-selectable="" data-value="AS">American Samoa</div><div class="option" data-selectable="" data-value="AD">Andorra</div><div class="option" data-selectable="" data-value="AO">Angola</div><div class="option" data-selectable="" data-value="AI">Anguilla</div><div class="option" data-selectable="" data-value="AQ">Antarctica</div><div class="option" data-selectable="" data-value="AG">Antigua and Barbuda</div><div class="option" data-selectable="" data-value="AR">Argentina</div><div class="option" data-selectable="" data-value="AM">Armenia</div><div class="option" data-selectable="" data-value="AW">Aruba</div><div class="option" data-selectable="" data-value="AU">Australia</div><div class="option" data-selectable="" data-value="AT">Austria</div><div class="option" data-selectable="" data-value="AZ">Azerbaijan</div><div class="option" data-selectable="" data-value="BS">Bahamas</div><div class="option" data-selectable="" data-value="BH">Bahrain</div><div class="option" data-selectable="" data-value="BD">Bangladesh</div><div class="option" data-selectable="" data-value="BB">Barbados</div><div class="option" data-selectable="" data-value="BY">Belarus</div><div class="option" data-selectable="" data-value="BE">Belgium</div><div class="option" data-selectable="" data-value="BZ">Belize</div><div class="option" data-selectable="" data-value="BJ">Benin</div><div class="option" data-selectable="" data-value="BM">Bermuda</div><div class="option" data-selectable="" data-value="BT">Bhutan</div><div class="option" data-selectable="" data-value="BO">Bolivia, Plurinational State of</div><div class="option" data-selectable="" data-value="BA">Bosnia and Herzegovina</div><div class="option" data-selectable="" data-value="BW">Botswana</div><div class="option" data-selectable="" data-value="BV">Bouvet Island</div><div class="option" data-selectable="" data-value="BR">Brazil</div><div class="option" data-selectable="" data-value="IO">British Indian Ocean Territory</div><div class="option" data-selectable="" data-value="BN">Brunei Darussalam</div><div class="option" data-selectable="" data-value="BG">Bulgaria</div><div class="option" data-selectable="" data-value="BF">Burkina Faso</div><div class="option" data-selectable="" data-value="BI">Burundi</div><div class="option" data-selectable="" data-value="KH">Cambodia</div><div class="option" data-selectable="" data-value="CM">Cameroon</div><div class="option" data-selectable="" data-value="CA">Canada</div><div class="option" data-selectable="" data-value="CV">Cape Verde</div><div class="option" data-selectable="" data-value="KY">Cayman Islands</div><div class="option" data-selectable="" data-value="CF">Central African Republic</div><div class="option" data-selectable="" data-value="TD">Chad</div><div class="option" data-selectable="" data-value="CL">Chile</div><div class="option" data-selectable="" data-value="CN">China</div><div class="option" data-selectable="" data-value="CX">Christmas Island</div><div class="option" data-selectable="" data-value="CC">Cocos (Keeling) Islands</div><div class="option" data-selectable="" data-value="CO">Colombia</div><div class="option" data-selectable="" data-value="KM">Comoros</div><div class="option" data-selectable="" data-value="CG">Congo</div><div class="option" data-selectable="" data-value="CD">Congo, the Democratic Republic of the</div><div class="option" data-selectable="" data-value="CK">Cook Islands</div><div class="option" data-selectable="" data-value="CR">Costa Rica</div><div class="option" data-selectable="" data-value="CI">Côte d'Ivoire</div><div class="option" data-selectable="" data-value="HR">Croatia</div><div class="option" data-selectable="" data-value="CU">Cuba</div><div class="option" data-selectable="" data-value="CY">Cyprus</div><div class="option" data-selectable="" data-value="CZ">Czech Republic</div><div class="option" data-selectable="" data-value="DK">Denmark</div><div class="option" data-selectable="" data-value="DJ">Djibouti</div><div class="option" data-selectable="" data-value="DM">Dominica</div><div class="option" data-selectable="" data-value="DO">Dominican Republic</div><div class="option" data-selectable="" data-value="EC">Ecuador</div><div class="option" data-selectable="" data-value="EG">Egypt</div><div class="option" data-selectable="" data-value="SV">El Salvador</div><div class="option" data-selectable="" data-value="GQ">Equatorial Guinea</div><div class="option" data-selectable="" data-value="ER">Eritrea</div><div class="option" data-selectable="" data-value="EE">Estonia</div><div class="option" data-selectable="" data-value="ET">Ethiopia</div><div class="option" data-selectable="" data-value="FK">Falkland Islands (Malvinas)</div><div class="option" data-selectable="" data-value="FO">Faroe Islands</div><div class="option" data-selectable="" data-value="FJ">Fiji</div><div class="option" data-selectable="" data-value="FI">Finland</div><div class="option" data-selectable="" data-value="FR">France</div><div class="option" data-selectable="" data-value="GF">French Guiana</div><div class="option" data-selectable="" data-value="PF">French Polynesia</div><div class="option" data-selectable="" data-value="TF">French Southern Territories</div><div class="option" data-selectable="" data-value="GA">Gabon</div><div class="option" data-selectable="" data-value="GM">Gambia</div><div class="option" data-selectable="" data-value="GE">Georgia</div><div class="option" data-selectable="" data-value="DE">Germany</div><div class="option" data-selectable="" data-value="GH">Ghana</div><div class="option" data-selectable="" data-value="GI">Gibraltar</div><div class="option" data-selectable="" data-value="GR">Greece</div><div class="option" data-selectable="" data-value="GL">Greenland</div><div class="option" data-selectable="" data-value="GD">Grenada</div><div class="option" data-selectable="" data-value="GP">Guadeloupe</div><div class="option" data-selectable="" data-value="GU">Guam</div><div class="option" data-selectable="" data-value="GT">Guatemala</div><div class="option" data-selectable="" data-value="GG">Guernsey</div><div class="option" data-selectable="" data-value="GN">Guinea</div><div class="option" data-selectable="" data-value="GW">Guinea-Bissau</div><div class="option" data-selectable="" data-value="GY">Guyana</div><div class="option" data-selectable="" data-value="HT">Haiti</div><div class="option" data-selectable="" data-value="HM">Heard Island and McDonald Islands</div><div class="option" data-selectable="" data-value="VA">Holy See (Vatican City State)</div><div class="option" data-selectable="" data-value="HN">Honduras</div><div class="option" data-selectable="" data-value="HK">Hong Kong</div><div class="option" data-selectable="" data-value="HU">Hungary</div><div class="option" data-selectable="" data-value="IS">Iceland</div><div class="option" data-selectable="" data-value="IN">India</div><div class="option" data-selectable="" data-value="ID">Indonesia</div><div class="option" data-selectable="" data-value="IR">Iran, Islamic Republic of</div><div class="option" data-selectable="" data-value="IQ">Iraq</div><div class="option" data-selectable="" data-value="IE">Ireland</div><div class="option" data-selectable="" data-value="IM">Isle of Man</div><div class="option" data-selectable="" data-value="IL">Israel</div><div class="option" data-selectable="" data-value="IT">Italy</div><div class="option" data-selectable="" data-value="JM">Jamaica</div><div class="option" data-selectable="" data-value="JP">Japan</div><div class="option" data-selectable="" data-value="JE">Jersey</div><div class="option" data-selectable="" data-value="JO">Jordan</div><div class="option" data-selectable="" data-value="KZ">Kazakhstan</div><div class="option" data-selectable="" data-value="KE">Kenya</div><div class="option" data-selectable="" data-value="KI">Kiribati</div><div class="option" data-selectable="" data-value="KP">Korea, Democratic People's Republic of</div><div class="option" data-selectable="" data-value="KR">Korea, Republic of</div><div class="option" data-selectable="" data-value="KW">Kuwait</div><div class="option" data-selectable="" data-value="KG">Kyrgyzstan</div><div class="option" data-selectable="" data-value="LA">Lao People's Democratic Republic</div><div class="option" data-selectable="" data-value="LV">Latvia</div><div class="option" data-selectable="" data-value="LB">Lebanon</div><div class="option" data-selectable="" data-value="LS">Lesotho</div><div class="option" data-selectable="" data-value="LR">Liberia</div><div class="option" data-selectable="" data-value="LY">Libyan Arab Jamahiriya</div><div class="option" data-selectable="" data-value="LI">Liechtenstein</div><div class="option" data-selectable="" data-value="LT">Lithuania</div><div class="option" data-selectable="" data-value="LU">Luxembourg</div><div class="option" data-selectable="" data-value="MO">Macao</div><div class="option" data-selectable="" data-value="MK">Macedonia, the former Yugoslav Republic of</div><div class="option" data-selectable="" data-value="MG">Madagascar</div><div class="option" data-selectable="" data-value="MW">Malawi</div><div class="option" data-selectable="" data-value="MY">Malaysia</div><div class="option" data-selectable="" data-value="MV">Maldives</div><div class="option" data-selectable="" data-value="ML">Mali</div><div class="option" data-selectable="" data-value="MT">Malta</div><div class="option" data-selectable="" data-value="MH">Marshall Islands</div><div class="option" data-selectable="" data-value="MQ">Martinique</div><div class="option" data-selectable="" data-value="MR">Mauritania</div><div class="option" data-selectable="" data-value="MU">Mauritius</div><div class="option" data-selectable="" data-value="YT">Mayotte</div><div class="option" data-selectable="" data-value="MX">Mexico</div><div class="option" data-selectable="" data-value="FM">Micronesia, Federated States of</div><div class="option" data-selectable="" data-value="MD">Moldova, Republic of</div><div class="option" data-selectable="" data-value="MC">Monaco</div><div class="option" data-selectable="" data-value="MN">Mongolia</div><div class="option" data-selectable="" data-value="ME">Montenegro</div><div class="option" data-selectable="" data-value="MS">Montserrat</div><div class="option" data-selectable="" data-value="MA">Morocco</div><div class="option" data-selectable="" data-value="MZ">Mozambique</div><div class="option" data-selectable="" data-value="MM">Myanmar</div><div class="option" data-selectable="" data-value="NA">Namibia</div><div class="option" data-selectable="" data-value="NR">Nauru</div><div class="option" data-selectable="" data-value="NP">Nepal</div><div class="option" data-selectable="" data-value="NL">Netherlands</div><div class="option" data-selectable="" data-value="AN">Netherlands Antilles</div><div class="option" data-selectable="" data-value="NC">New Caledonia</div><div class="option" data-selectable="" data-value="NZ">New Zealand</div><div class="option" data-selectable="" data-value="NI">Nicaragua</div><div class="option" data-selectable="" data-value="NE">Niger</div><div class="option" data-selectable="" data-value="NG">Nigeria</div><div class="option" data-selectable="" data-value="NU">Niue</div><div class="option" data-selectable="" data-value="NF">Norfolk Island</div><div class="option" data-selectable="" data-value="MP">Northern Mariana Islands</div><div class="option" data-selectable="" data-value="NO">Norway</div><div class="option" data-selectable="" data-value="OM">Oman</div><div class="option" data-selectable="" data-value="PK">Pakistan</div><div class="option" data-selectable="" data-value="PW">Palau</div><div class="option" data-selectable="" data-value="PS">Palestinian Territory, Occupied</div><div class="option" data-selectable="" data-value="PA">Panama</div><div class="option" data-selectable="" data-value="PG">Papua New Guinea</div><div class="option" data-selectable="" data-value="PY">Paraguay</div><div class="option" data-selectable="" data-value="PE">Peru</div><div class="option" data-selectable="" data-value="PH">Philippines</div><div class="option" data-selectable="" data-value="PN">Pitcairn</div><div class="option" data-selectable="" data-value="PL">Poland</div><div class="option" data-selectable="" data-value="PT">Portugal</div><div class="option" data-selectable="" data-value="PR">Puerto Rico</div><div class="option" data-selectable="" data-value="QA">Qatar</div><div class="option" data-selectable="" data-value="RE">Réunion</div><div class="option" data-selectable="" data-value="RO">Romania</div><div class="option" data-selectable="" data-value="RU">Russian Federation</div><div class="option" data-selectable="" data-value="RW">Rwanda</div><div class="option" data-selectable="" data-value="BL">Saint Barthélemy</div><div class="option" data-selectable="" data-value="SH">Saint Helena, Ascension and Tristan da Cunha</div><div class="option" data-selectable="" data-value="KN">Saint Kitts and Nevis</div><div class="option" data-selectable="" data-value="LC">Saint Lucia</div><div class="option" data-selectable="" data-value="MF">Saint Martin (French part)</div><div class="option" data-selectable="" data-value="PM">Saint Pierre and Miquelon</div><div class="option" data-selectable="" data-value="VC">Saint Vincent and the Grenadines</div><div class="option" data-selectable="" data-value="WS">Samoa</div><div class="option" data-selectable="" data-value="SM">San Marino</div><div class="option" data-selectable="" data-value="ST">Sao Tome and Principe</div><div class="option" data-selectable="" data-value="SA">Saudi Arabia</div><div class="option" data-selectable="" data-value="SN">Senegal</div><div class="option" data-selectable="" data-value="RS">Serbia</div><div class="option" data-selectable="" data-value="SC">Seychelles</div><div class="option" data-selectable="" data-value="SL">Sierra Leone</div><div class="option" data-selectable="" data-value="SG">Singapore</div><div class="option" data-selectable="" data-value="SK">Slovakia</div><div class="option" data-selectable="" data-value="SI">Slovenia</div><div class="option" data-selectable="" data-value="SB">Solomon Islands</div><div class="option" data-selectable="" data-value="SO">Somalia</div><div class="option" data-selectable="" data-value="ZA">South Africa</div><div class="option" data-selectable="" data-value="GS">South Georgia and the South Sandwich Islands</div><div class="option" data-selectable="" data-value="ES">Spain</div><div class="option" data-selectable="" data-value="LK">Sri Lanka</div><div class="option" data-selectable="" data-value="SD">Sudan</div><div class="option" data-selectable="" data-value="SR">Suriname</div><div class="option" data-selectable="" data-value="SJ">Svalbard and Jan Mayen</div><div class="option" data-selectable="" data-value="SZ">Swaziland</div><div class="option" data-selectable="" data-value="SE">Sweden</div><div class="option" data-selectable="" data-value="CH">Switzerland</div><div class="option" data-selectable="" data-value="SY">Syrian Arab Republic</div><div class="option" data-selectable="" data-value="TW">Taiwan, Province of China</div><div class="option" data-selectable="" data-value="TJ">Tajikistan</div><div class="option" data-selectable="" data-value="TZ">Tanzania, United Republic of</div><div class="option" data-selectable="" data-value="TH">Thailand</div><div class="option" data-selectable="" data-value="TL">Timor-Leste</div><div class="option" data-selectable="" data-value="TG">Togo</div><div class="option" data-selectable="" data-value="TK">Tokelau</div><div class="option" data-selectable="" data-value="TO">Tonga</div><div class="option" data-selectable="" data-value="TT">Trinidad and Tobago</div><div class="option" data-selectable="" data-value="TN">Tunisia</div><div class="option" data-selectable="" data-value="TR">Turkey</div><div class="option" data-selectable="" data-value="TM">Turkmenistan</div><div class="option" data-selectable="" data-value="TC">Turks and Caicos Islands</div><div class="option" data-selectable="" data-value="TV">Tuvalu</div><div class="option" data-selectable="" data-value="UG">Uganda</div><div class="option" data-selectable="" data-value="UA">Ukraine</div><div class="option" data-selectable="" data-value="AE">United Arab Emirates</div><div class="option" data-selectable="" data-value="GB">United Kingdom</div><div class="option" data-selectable="" data-value="US">United States</div><div class="option" data-selectable="" data-value="UM">United States Minor Outlying Islands</div><div class="option" data-selectable="" data-value="UY">Uruguay</div><div class="option" data-selectable="" data-value="UZ">Uzbekistan</div><div class="option" data-selectable="" data-value="VU">Vanuatu</div><div class="option" data-selectable="" data-value="VE">Venezuela, Bolivarian Republic of</div><div class="option" data-selectable="" data-value="VN">Viet Nam</div><div class="option" data-selectable="" data-value="VG">Virgin Islands, British</div><div class="option" data-selectable="" data-value="VI">Virgin Islands, U.S.</div><div class="option" data-selectable="" data-value="WF">Wallis and Futuna</div><div class="option" data-selectable="" data-value="EH">Western Sahara</div><div class="option" data-selectable="" data-value="YE">Yemen</div><div class="option" data-selectable="" data-value="ZM">Zambia</div><div class="option" data-selectable="" data-value="ZW">Zimbabwe</div></div></div><div class="selectize-dropdown single demo-default" style="display: none; width: 243px; top: 511px; left: 655px; visibility: visible;"><div class="selectize-dropdown-content"><div class="option" data-selectable="" data-value="4">Accounting</div><div class="option" data-selectable="" data-value="5">Banking</div><div class="option selected active" data-selectable="" data-value="1">IT &amp; Software</div><div class="option" data-selectable="" data-value="3">Marketing</div></div></div>
    <script src="{{ asset('FrontEnd')}}/js/home.js"></script>


</body></html>